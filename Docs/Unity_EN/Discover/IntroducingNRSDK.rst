.. role:: raw-html-m2r(raw)
   :format: html

.. _introducing_NRSDK:

Introducing NRSDK
=================

:raw-html-m2r:`<font color=#636363>NRSDK is Nreal’s platform for developing mixed reality experiences. Using a simple development process and a high-level API, NRSDK provides a set of powerful MR features and enables your Nreal glasses to understand the real world.</font>`

:raw-html-m2r:`<br />`


|image0|


NRSDK provides five :ref:`Core Features<core_features>` for developers:


* **Spatial Computing** :raw-html-m2r:`<font color=#636363>allows the glasses to track their real-time position relative to the world and understand the environment around them, such as detecting and tracking planar surfaces and images.</font>`
* **Optimized Rendering**\ :raw-html-m2r:`<font color=#636363> is automatically applied to the applications and runs in the backend to minimize latency and reduce judder, enhancing the overall user experience.</font>`
* **Multi-modal Interactions** :raw-html-m2r:`<font color=#636363>provide intuitive choices of interaction for different use cases.</font>`
* **Developer Tools** :raw-html-m2r:`<font color=#636363>are provided so you can better develop and debug apps.</font>`
* **Third-party Integration** :raw-html-m2r:`<font color=#636363>is achieved by providing data for third-party SDKs, which allows you to fully utilize Nreal Light’s hardware features and build powerful MR/AR applications.</font>`
  :raw-html-m2r:`<br />`

Discover more NRSDK :ref:`Core Features<core_features>`.

Nreal Developer Kit
^^^^^^^^^^^^^^^^^^^


|image1|


An Nreal Developer Kit is required for developing mixed reality apps. The Developer Kit consists of a pair of Nreal Light glasses, an Nreal computing unit, and an Nreal Light controller. If you do not have one, Sign up for the \ `Nreal Developer Kit <https://developer.nreal.ai/login>`_\  here!



Choose a Development Platform
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>NRSDK supports many of the most popular development environments. With these capabilities, you can build entirely new MR experiences or enhance existing native Android apps with MR features.</font>`


|image2|


**Unity** (Support Unity 2018.2.X or later)

**Android Native**\ :raw-html-m2r:`<font color=#636363> (to be released)</font>`

**Unreal** :raw-html-m2r:`<font color=#636363>(to be released)</font>`

:raw-html-m2r:`<br />`

Compatible with Android Native App
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>Nreal glasses are now compatible with Android native apps, which means as long as the app is installed on the devices, users can view all app activity through their glasses. Nothing needs to be changed on your end. To make the 2D app more immersive and three-dimensional, you can use NRSDK to add on MR features or 3D virtual objects to the existing app.</font>`

:raw-html-m2r:`<br />`




.. |image0| image:: ../../../images/Discover/introduction-nrsdk01.jpg
.. |image1| image:: ../../../images/Discover/introduction-nrsdk02.png
.. |image2| image:: ../../../images/Discover/introduction-nrsdk03.jpg