.. role:: raw-html-m2r(raw)
   :format: html

.. _core_features:

Core Features
=============

:raw-html-m2r:`<font color=#636363>Let’s get started with NRSDK's five core features. They are</font>` **Spacial Computing**\ , **Optimized Rendering**\ , **Multi-modal Interactions**\ , **Developer Tools** :raw-html-m2r:`<font color=#636363>and</font>` **Third-party Integration**.

:raw-html-m2r:`<br />`

Spatial Computing
^^^^^^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>Nreal glasses use a variety of sensors and cameras to build a sophisticated understanding of both the surrounding environment and the users themselves, creating immersive experiences that seamlessly blend the digital world with the real world.</font>`
:raw-html-m2r:`<br />`


|image0|

:raw-html-m2r:`<br />`


* **6DoF Tracking** :raw-html-m2r:`<font color=#636363>technology uses the two SLAM cameras located on both sides of the Nreal glasses to identify feature points, tracking how these points move over time. Combining the movement of these points with readings from the glasses' IMU sensors, NRSDK accurately tracks both the position and orientation of the glasses as it moves through space. 6DoF tracking also provides developers with real-time mapping constructions and 3D point clouds, giving the applications information on the physical structures of the environment.</font>`
  :raw-html-m2r:`<br />`


.. image:: https://nreal-public.nreal.ai/developer/img/corefeature02.gif


:raw-html-m2r:`<br />`


* **Plane Detection** :raw-html-m2r:`<font color=#636363>enables Nreal glasses to detect flat surfaces (both horizontal and vertical) in the environment, such as a table or a wall (in NRSDK 1.0.0 beta, only horizontal plane detection is available). The transformation of the plane is continuously updated. When the glasses move around, the plane can be extended, and multiple planes can merge into one when they overlap.</font>`
  :raw-html-m2r:`<br />`


.. image:: https://nreal-public.nreal.ai/developer/img/corefeature03.gif


:raw-html-m2r:`<br />`


* **Image Tracking** :raw-html-m2r:`<font color=#636363>allows apps to recognize physical images and build augmented reality experiences around them. By adding multiple reference images to the image database, more than one pre-trained image can be detected in a session.</font>`

:raw-html-m2r:`<br />`

Optimized Rendering
^^^^^^^^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>NRSDK optimizes the rendering performance in the backend to minimize the latency of the entire system and to reduce judder. It provides a smooth and comfortable user experience devoid of dizziness and sickness. You do not need to enable or tune rendering features as they are automatically applied. </font>`


* **Warping**
  :raw-html-m2r:`<font color=#636363>Instead of polling tracking data at the very beginning of each frame to render the image, NRSDK uses the last predicted position of the Nreal glasses to warp the rendered image, reproject, and send to display before every VSync.</font>`

:raw-html-m2r:`<br />`

Multi-modal Interactions
^^^^^^^^^^^^^^^^^^^^^^^^


|image3|


* 
  **Nreal Light Controller (3DoF)**
  A light 3DoF controller to interact with the virtual objects in a natural and intuitive way.

* 
  **Nreal Phone Controller (3DoF)** (Currently is not available for you to develop apps) An Android mobile phone can serve as a 3DoF controller when connected to Nreal glasses. The touchscreen supports gestures and all interactions mirror interactions on the Nreal controller.

 For more interaction models,  please refer to the `Design Guide <https://developer.nreal.ai/design/interacting>`_ .

:raw-html-m2r:`<br />`

Developer Tools
^^^^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>Developer Tools, such as</font>`  **Testing Tool** and **Observer View**\ :raw-html-m2r:`<font color=#636363>, are something we designed especially for you.</font>`


* 
  The **Testing Tool** (will be available in next NRSDK version) is like an emulator running under the Unity Editor play mode. Inputs that would usually be read by the sensors on Nreal glasses are instead simulated using the keyboard, mouse, or smartphone. It comes with NRSDK and allows you to test applications without having real Nreal devices.
  

* 
  **Observer View** :raw-html-m2r:`<font color=#636363>allows you to share your creations with others by allowing them to see a Nreal Light user's world on a 2D phone or tablet screen, which is synchronized by a marker.</font>`

:raw-html-m2r:`<br />`

Third-party Integration
^^^^^^^^^^^^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>NRSDK is designed as an open platform and can be integrated with third-party SDKs as plug-ins. For example, data from the RGB camera is available for third-party SDKs to run face detection algorithms.</font>`

:raw-html-m2r:`<br />`

.. |image0| image:: ../../../images/Discover/core-feature01.gif
.. |image3| image:: ../../../images/Discover/core-feature04.jpg
