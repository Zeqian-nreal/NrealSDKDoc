
Mapping
=======

Operating environment
---------------------

* 【Hardware】 PC/Android mobile phone + Adapted mobile phone or Compute Unit + Nreal Light
* 【Version】 SDK:v1.5.7_beta and above 


Import NRSDKBeta Package
------------------------

Import latest SDKBeta version package.

Open the Sample Scene
---------------------

In the Unity Project window, you can find the CameraCaptureDemo sample in: **Assets > NRSDKBeta >Demos > MappingExample**.

Use the NRWorldAnchorStore step by step
---------------------------------------

..

   See ``CameraCaptureController.cs`` , located in ``Assets > NRSDKBeta > Demos > Mapping > Scripts > LocalMapExample`` for an example on how to use the video capture. 


* To begin with creating an instance of NRWorldAnchorStore, SDK will load the offline map file from the path **(/sdcard/Android/data/{your app's name}/files/NrealMaps)** automatically and return the value of instance asynchronously.

    .. code-block:: c#

        // Create NRWorldAnchorStore object.
        public void Load()
        {
            NRWorldAnchorStore.GetAsync(GetAnchorStoreCallBack);
        }

        private void GetAnchorStoreCallBack(NRWorldAnchorStore store)
        {
            myAnchorStore = store;
        }


* Use the GetAllIds() method of NRWorldAnchorStore to get information about all anchors (NRWorldAnchor objects, hereinafter referred to as anchors) under the current map. This anchor is the anchor added and saved during the last run of the program. Use the Load (key, behaviour) interface according to the key value to Load the corresponding object, instantiation of objects will automatically be placed as a child of the anchor which will update its pose information every frame.
 
    .. code-block:: c#
       
        var keys = myAnchorStore.GetAllIds();
        if (keys != null)
        {
            foreach (var key in myAnchorStore.GetAllIds())
            {
                // Instantiate the gameobject by key.
                GameObject prefab;
                if (m_AnchorPrefabDict.TryGetValue(key, out prefab))
                {
                    var go = Instantiate(prefab);
                    myAnchorStore.Load(key, go);
                    m_LoadedAnchorDict[key] = go;
                }
            }
        }


* To delete all anchors, please utilize the method myAnchorStore.Clear(). Once executing this function, the project can't load any anchor date.

    .. code-block:: c#

        myAnchorStore.Clear();

* To add more anchors, please utilize Load() interface and income the value of the key and an instance of gameobject. By utilizing this interface, NRSDK will create an anchor according to the position of the gameobject and put the instance of the gameobject under the node of this anchor. If the key-value belongs to one of the previous anchors, the previous one and its child objects will be deleted.

    .. code-block:: c#

        var go = Instantiate(target.gameObject);
        go.transform.position = target.position;
        go.transform.rotation = target.rotation;
        go.SetActive(true);

        string key = go.GetComponent<AnchorItem>().key;
        bool result = m_NRWorldAnchorStore.Load(key, go);

* To save all anchors in the storage, please utilize the method myAnchorStore.Save (). Once it saves the anchors, the program will load the objects according to keys in the next runs

    .. code-block:: c#

        myAnchorStore.Save();

Run your build application
--------------------------


* Name the offline map as "nreal_map.dat" and push it under the folder /sdcard/Android/data/{your app's name}/files/NrealMaps. Please create the folder if you don't have one.
* Run the app and click the load button to load the map.
* Click "OpenAnchorPanel" button to open a panel. Put the anchor model in the same position as the anchor in the map and click the "Save"button and exit the app.
* For the following times: If you are in the same environment at where the map is captured before, the NRSDK will detect the environment and display the virtual objects after executing "load"
